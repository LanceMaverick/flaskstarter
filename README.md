# Example and starter template for a flask app.
This project can be used as an example or a starting template for a fully featured flask app.

Includes:
- login view and form
- user and role management
- example forms
- example models
- example views, blueprints and templates
- database setup with alembic migrations
- full app `__init__.py`

## Root Folder
The root folder contains the management tools for creating db migrations, 
managing users and running the app, either through wsgi or the flask dev server

### manage.py
init, migrate and upgrade db. e.g:
```python manage.py db init```

### manage_users.py
Create, edit and delete users and roles. e,g:
``` python manage_users.py create```

### run.py/wsgi.py
Run the server. e.g:
```python run.py```
These files are identical in the example, but kept separately in case different
conditions are wanted for using the run.py (meant for the dev server) and wsgi.

## myapp
This folder is the app module. the `__init__.py` has all the app setup such as static folder
locations, app config, blueprint importing and app creation

### auth.py
Authentication related views and decorators

### config.py
Committed in the example but is also in the .gitignore. Here will be any API keys, password salt, flask secret key etc and must be kept private

### datapase.py
Creation of the `db` object, which is using *SQAlchemy*

### forms.py
Examples of easy form creation using `FlaskForm` from *flask-wtf*. These forms are python classes and can be
easily manipulated in the flask views, and passed and rendered into the templates.

### logconf.py
Configuration file for the app logging. Output format and other options can be changed here.
Loggin in the app is done like so:

```python
from flask import current_app as app
app.logger.info('message') #info, debug, error,...
```

### models.py

The schema for the database tables is defined my the classes in this file, which inherit from db.Models.
When migrating changes in the db, the sql commands are automatically created and run based on these classes. All migration files should be reviewed and edited before upgrading however, as fine changes such as a column type will have to be added manually (but with python). See https://flask-migrate.readthedocs.io/en/latest/.

### myviews.py
An example views file for the "myviews" blueprint. Includes an example index page that lists all usernames, and an example contact view.

### templates folder
All jinja2 templates are stored here, with each folder being a different blueprint.

### static folder
css, javascript and the instance folder (containing assets that are instance only, such as uploaded files) stored here. css is built from the `app.scss` file automatically by *flask-scss*

# Simple Recipes

## Creating a view that serves a form that will commit an entry to the database when submitted:

### models.py
```python
from .database import db

class Contact(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String())
    message = db.Column(db.Text())
```

### forms.py
```python
from myapp.models import Contact
from wtforms.ext.sqlalchemy.orm import model_form

ContactForm = model_form(Contact)
```

### views.py
```python
from flask import Blueprint, redirect, render_template
from myapp.models import Contact
from myapp import forms 
from .database import db

bp = Blueprint('myviews', __name__)
@bp.contact('/contact')
def contact():
    form = forms.ContactForm(request.form)
        if form.validate_on_submit():
            new_contact = Contact()
            form.populate_obj(new_contact)
            db.session.add(new_contact)
            db.session.commit()
            return redirect(url_for('myviews.index'))
    return render_template('myviews/contact.html', form=form)
```
### templates/myviews/contact.html
```html
{% extends 'base.html' %}
{% import 'bootstrap/wtf.html' as wtf %} 

{% block header %}
{% endblock %}

{% block content %}
    {{ wtf.quick_form(form) }} 
{% endblock %}
```
## Protecting a view to be admin only

### views.py
```python
from flask import Blueprint, redirect, render_template
from myapp.auth import admin_required

@admin_required
@bp.route('/dashboard')
def dashboard():
    #admin only stuff
    return render_template('admin/dashboard.html')
```

## Creating a file upload form

### forms.py
```python
from flask_wtf import FlaskForm
from wtforms import SubmitField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES

images = UploadSet('images', IMAGES)

class UploadImageForm(FlaskForm):
    image = FileField(u'Image', validators=[FileRequired(), FileAllowed(images, 'Only image files are allowed.')])
    submit = SubmitField('Upload')
```

### views.py
```python
# usual blueprint setupand flask view related imports

@bp.route('/upload', methods=['GET', 'POST'])
def upload():
    form = forms.UploadImageForm()
    if form.validate.on_submit():
        filename = secure_filename(form.image.data.filename)
        #can use, for example uuid.uuid4().hex+filename to make unique filename
        #save file or do whatever with it, e.g:
        save_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        form.award_img.data.save(save_path) #save image
        return redirect(url_for('index'))
    return render_template('upload.html', form=form)
```
### upload.html
```html
{% extends 'base.html' %}
{% import 'bootstrap/wtf.html' as wtf %} 

{% block content %}
    <div class="container">
       <!-- wtf automatically sets multipart data if FileField detected -->
        {{ wtf.quick_form(form) }}
    </div>
{% endblock %}
```
## Create simple API endpoint
### api.py
```python
from flask import Blueprint, request, jsonify, Response
from flask_cors import cross_origin

bp = Blueprint('api', __name__, url_prefix='/api')

#get example
@bp.route('/getdata', methods=['GET'])
@cross_origin()
def data():
    #get some data from somewhere, e.g the db
    data = some_data()
    return jsonify(data)
#will get data json when requesting mydomain.com/api/getdata

#put example
@bp.route('/putdata', methods=['PUT'])
def put_data():
    data = json.loads(request.data) # get data from request
    key = data['key'] #using simple api key auth
    myvar = data['myvar']

    if key not in some_container_of_auth_keys:
        return Response('Access Denied', 401, {'WWW-Authenticate':'Basic realm="Login Required"'})
    else:
        put_data_somewhere(myvar)
        
    return Response(r.status_code)
```





