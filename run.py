from myapp.logconf import configure_logging
from myapp import create_app
configure_logging()
app = create_app()

if __name__ == "__main__":
    app.run()
