import sys
import json
import datetime

# commonly used flask imports
from flask import (
    Blueprint, flash, redirect, render_template, request, url_for, send_from_directory
)

#needed for logging within the app
from flask import current_app as app

#can be used to modify the request if client is on mobile
import flask_mobility

#used for security when getting the filename of an uploaded file
from werkzeug import secure_filename

#login_required decorator to protect views that need the user to be logged in.
#redirects to login page if not. 
from flask_security import login_required

#current_user is useful for accessing the current logged in user
from flask_security current_user

#for queries and entries
from myapp.models import User

#ready made forms
from myapp import forms 

#for db transations
from .database import db

#best to split site sections by blueprint/python file
#e.g register and login views can go in auth.py using
#the blueprint "auth". Here is an example blueprint called myviews.

bp = Blueprint('myviews', __name__)

@bp.route('/')
def index():
    users = User.query.all()
    return render_template('myviews/index.html', users = users)

@bp.contact('/contact')
def contact():
    form = forms.ContactForm(request.form)
        if form.validate_on_submit():
            new_contact = Contact()
            form.populate_obj(new_contact)
            db.session.add(new_contact)
            db.session.commit()
            return redirect(url_for('myviews.index'))
    return render_template('myviews/contact.html', form=form)







