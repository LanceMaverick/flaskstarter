import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from flask_security import login_user, logout_user, current_user, login_required
from flask_security.forms import ChangePasswordForm
from flask import current_app as app

from myapp.models import User
from .database import db

bp = Blueprint('auth', __name__, url_prefix='/auth')

#TODO make example register view
@bp.route('/register')
def register():
    return "Registering is handled manually by the admin."

#example login view
@bp.route('/login', methods=('GET', 'POST'))
def login():
    #next param is used to redirect user to previously requested page after login
    next_param = request.args.get('next')
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        error = None
        user = User.query.filter_by(username=username).first()
        if user is None or not user.check_password(password):
            flash('Incorrect username or password.','danger')
            return redirect(url_for('auth.login', next=next_param))
        login_user(user)
        if next_param:
                return redirect(next_param)

        return redirect(url_for('index', error=error))

    return render_template('auth/login.html')

#example accouunt page allowing user to change their password
@login_required
@bp.route('/account', methods=('GET', 'POST'))
def account():
    user = current_user
    form = ChangePasswordForm(request.form)
    if request.method =='POST':
        if form.validate():
            password = form.new_password.data
            user.set_password(password)
            db.session.commit()
            flash('Password updated!', 'success')
        else:
            flash('ERROR', 'danger')
            for e in form.password.errors:
                flash(e, 'danger')
    return render_template('auth/account.html', form=form)

@bp.before_app_request
def load_logged_in_user():
    g.user = current_user

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@bp.route('/denied')
def denied():
    return render_template('auth/denied.html')

#example decorator to check the user has the admin role
def admin_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('admin'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view
