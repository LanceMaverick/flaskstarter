from datetime import datetime
from .database import db
from flask_security.utils import hash_password, verify_and_update_password
from flask_security import UserMixin, RoleMixin, utils


#relationship table for roles and users
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

#User model for accounts. Password management is done via methods
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False, index=True)
    password = db.Column(db.String(), nullable=False)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    
    def check_password(self, password):
        return verify_and_update_password(password, self)
    
    def set_password(self, password):
        self.password = hash_password(password)

    def __repr__(self):
        return '<User %r>' % self.username
    
#Roles are useful for member access. e.g, admin or moderator roles
class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))
    def __repr__(self):
        return '<Role %r>' % self.name

#example model for storing messages sent via a contact form
class Contact(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String())
    message = db.Column(db.Text())
    def __repr__(self):
        return '<Message %r>' % self.id