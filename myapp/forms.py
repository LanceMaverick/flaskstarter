from flask_wtf import FlaskForm

#for the creation of forms directly from db models
from wtforms.ext.sqlalchemy.orm import model_form

#commonly used form fields:
from wtforms import (SubmitField, StringField, PasswordField, SelectField, 
SubmitField, TextAreaField, BooleanField, HiddenField, EmailField, validators)
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired

#for file uploads in forms:
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES

#example form:
class ContactForm(FlaskForm):
    name = StringField(u'Name', [validators.required(), validators.length(max=100)])
    email = EmailField('Email address', [validators.DataRequired(), validators.Email()])
    message = TextAreaField(u'Message', [validators.required(), validators.length(max=1000)])

#Since this form is exactly like the database model that will be filled from it,
# (see Contact in models.py), it can also be created easily using model_form like so:

'''
from myapp.models import Contact
ContactForm = model_form(Contact)
'''

